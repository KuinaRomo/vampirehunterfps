﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class moveZombie : MonoBehaviour {

    private bool move;
    private GameObject objectivePoint;
    private GameObject pointManager;
    private GameObject Player;
    private GameObject barraVida;
    private GameObject canvas;
    [SerializeField] GameObject cohetes;
    [SerializeField] GameObject botiquin;
    [SerializeField] GameObject fireEffect;
    public bool day;
    public bool atacando;
    private bool firstPoint;
    private bool elegido;
    private float totalTime;
    private float contador;
    public float speed;
    public float count;
    
	// Use this for initialization
	void Start () {
        move = false;
        firstPoint = false;
        day = false;
        atacando = false;
        Player = GameObject.FindGameObjectWithTag("Player");
        pointManager = GameObject.FindGameObjectWithTag("pointManager");
        barraVida = GameObject.FindGameObjectWithTag("canvasImage");
        canvas = GameObject.FindGameObjectWithTag("canvas");
        totalTime = 15f;
        speed = 0.5f;
        count = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (move) {
            goToNext();
            if (atacando) {
                count += Time.deltaTime;
            }
        }
        
    }

    public void moveZombieNow() {
        move = true;
    }

    private void goToNext() {
        GetComponent<NavMeshAgent>().destination = Player.transform.position;
    }

    public void quitarVida() {
        Debug.Log(count);
        Debug.Log(atacando);
        if(count > 0.5f) { 
            barraVida.GetComponent<Image>().fillAmount -= 0.2f;
            if(barraVida.GetComponent<Image>().fillAmount <= 0.1f) {
                //Llamar funcion que muestre has muerto.
                canvas.GetComponent<canvasController>().GameOver = true;
            }
            count = 0;
        }
    }

    public void deleteZombie() {
        instanceObjects();
        if (day) {
            GameObject fire = Instantiate(fireEffect, this.transform.position, Quaternion.identity);
            fire.AddComponent<RemoveExplosion>();
        }
        Destroy(this.gameObject);
    }

    public void instanceObjects() {
        float randomNum = Random.Range(0, 100);
        if (randomNum >= 80) {
            randomNum = Random.Range(0, 2);
            if (randomNum == 0) {
                Instantiate(cohetes, this.transform.position, this.transform.rotation);
            }
            else {
                Instantiate(botiquin, this.transform.position, this.transform.rotation);
            }
        }
    }
}
