﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWeapon : MonoBehaviour {

    bool ballesta;
    [SerializeField] GameObject crossbow;
    [SerializeField] GameObject rocketLaucher;

    // Use this for initialization
    void Start () {
        ballesta = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0)) {
            if (ballesta) {
                crossbow.SetActive(false);
                rocketLaucher.SetActive(true);
                ballesta = false;
            }
            else {
                crossbow.SetActive(true);
                rocketLaucher.SetActive(false);
                ballesta = true;
            }
        }
	}
}
