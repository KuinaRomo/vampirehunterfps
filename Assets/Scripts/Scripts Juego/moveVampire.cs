﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class moveVampire : MonoBehaviour {

    public GameObject point;
    GameObject pointManager;
    [SerializeField] GameObject Player;
    private GameObject barraVida;
    [SerializeField] GameObject cohetes;
    [SerializeField] GameObject botiquin;
    [SerializeField] GameObject fireEffect;
    GameObject canvas;
    public float count;
    private bool firstPoint;
    public bool elegido;
    public bool atacando;
    private float totalTime;
    private float contador;
    public float speed;
    private float randomNum;
    private bool objectInstantiated;
    public bool day;
    //[SerializeField] BoxCollider heart;

    // Use this for initialization
    void Start () {
        day = false;
        firstPoint = false;
        elegido = false;
        objectInstantiated = false;
        atacando = false;
        pointManager = GameObject.FindGameObjectWithTag("pointManager");
        barraVida = GameObject.FindGameObjectWithTag("canvasImage");
        Player = GameObject.FindGameObjectWithTag("Player");
        canvas = GameObject.FindGameObjectWithTag("canvas");
        totalTime = 15f;
        contador = 0;
        randomNum = 0;
        speed = 1;
        count = 0;
    }
	
	// Update is called once per frame
	void Update () {
        /*if(this.GetComponent<NavMeshAgent>().remainingDistance < 0.5f && elegido == false) {
             elegido = true;
             point = pointManager.GetComponent<MoveRandom>().changePoint(point, firstPoint);
             if (!firstPoint) {
                 firstPoint = true;                
             }
             move();
         }

         if (elegido) {
             contador += Time.deltaTime;
             if(contador > totalTime) {
                 contador = 0;
                 elegido = false;
             }
         }*/
        move();
        if (atacando) {
            count += Time.deltaTime;
        }
 	}

    public void deleteVampire() {
        instanceObjects();
        if (day) {
            GameObject fire = Instantiate(fireEffect, this.transform.position, Quaternion.identity);
            fire.AddComponent<RemoveExplosion>();
        }
        Destroy(this.gameObject);
    }

    public void instanceObjects() {
        float randomNum = Random.Range(0, 100);
        if (randomNum >= 80) {
            randomNum = Random.Range(0, 2);
            if(randomNum == 0) {
                Instantiate(cohetes, this.transform.position, this.transform.rotation);
            }
            else {
                Instantiate(botiquin, this.transform.position, this.transform.rotation);
            }
        }
    }

    public void move() {
        //GetComponent<NavMeshAgent>().destination = point.transform.position;
        GetComponent<NavMeshAgent>().destination = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    public void quitarVida() {
        if(count > 1) {
            count = 0;
            barraVida.GetComponent<Image>().fillAmount -= 0.2f;
            if (barraVida.GetComponent<Image>().fillAmount <= 0.1f) {
                //Llamar funcion que muestre has muerto.
                canvas.GetComponent<canvasController>().GameOver = true;
            }
        }
    }
}
