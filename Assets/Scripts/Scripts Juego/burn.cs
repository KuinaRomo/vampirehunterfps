﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class burn : MonoBehaviour {

    private GameObject canvas;
    private GameObject barraVida;

	// Use this for initialization
	void Start () {
        canvas = GameObject.FindGameObjectWithTag("canvas");
        barraVida = GameObject.FindGameObjectWithTag("canvasImage");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        Debug.Log("El tag de: " + other.gameObject.name + " is " + other.gameObject.tag);
        if(other.gameObject.tag == "zombie" || other.gameObject.tag == "vampire") {
            other.GetComponent<Animator>().SetBool("death", true);
            canvas.GetComponent<canvasController>().monstruosMatados += 1;
        }

        else if(other.gameObject.tag == "Player") {
            barraVida.GetComponent<Image>().fillAmount -= 0.1f;
        }
    }
}
