﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationCrossbow : MonoBehaviour {

    [SerializeField] Mesh arcoTenso;
    [SerializeField] Mesh arcoDisparado;
    private bool disparado;

    // Use this for initialization
    void Start () {
        disparado = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Z)) {
            if (!disparado) {
                GetComponent<MeshFilter>().mesh = arcoDisparado;
                disparado = true;
            }

            else {
                GetComponent<MeshFilter>().mesh = arcoTenso;
                disparado = false;
            }
        }
	}
}
