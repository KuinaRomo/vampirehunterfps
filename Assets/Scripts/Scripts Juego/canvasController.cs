﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class canvasController : MonoBehaviour {

    public int numMisiles;
    private int day;
    public int monstruosMatados;
    private float contador;
    private float totalTime;
    [SerializeField] GameObject misils;
    [SerializeField] GameObject dayText;
    [SerializeField] GameObject OverText;
    [SerializeField] GameObject result;
    [SerializeField] GameObject Player;
    [SerializeField] GameObject Ballesta;
    [SerializeField] GameObject Bazooka;
    [SerializeField] GameObject TOD;
    private bool quitado;
    private float Hour;
    private float slider;
    private float speed;
    private bool diaMostrado;
    public bool deathMonsters;
    public bool noche;
    public bool GameOver;
    private GameObject[] vampires;
    private GameObject[] zombies;
    private string dia;
    private string monstruo;

    // Use this for initialization
    void Start () {
        numMisiles = 3;
        day = 0;
        monstruosMatados = 0;
        contador = 5;
        totalTime = 5;
        quitado = false;
        diaMostrado = false;
        noche = false;
        dia = " dias ";
        monstruo = " monstruos.";
        mostrarDia();
        slider = 0.8f;
        speed = 1000;
        misils.GetComponent<Text>().text = "x " + numMisiles;
    }

    private void OnGUI() {
        Hour = slider * 24;
        slider = slider + Time.deltaTime / speed;
        if(slider >= 1) {
            slider = 0;
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (GameOver) {
                SceneManager.LoadScene("MenuPrincipal");
                speed = 0;
                TOD.SetActive(false);
            }
        }

        if (Hour >= 18 || Hour <= 6)  {
            speed = 1000;
            diaMostrado = false;
            noche = true;
        }

        else {
            speed = 200;
            //Debug.Log("Matar a todos los monstruos");
            if (!diaMostrado) {
                noche = false;
                vampires = GameObject.FindGameObjectsWithTag("vampire");
                zombies = GameObject.FindGameObjectsWithTag("zombie");
                for (int i = 0; i < vampires.Length; i++) {
                    vampires[i].GetComponent<moveVampire>().day = true;
                    vampires[i].GetComponent<moveVampire>().deleteVampire();
                }
                for (int i = 0; i < zombies.Length; i++) {
                    zombies[i].GetComponent<moveZombie>().day = true;
                    zombies[i].GetComponent<moveZombie>().deleteZombie();
                }
                mostrarDia();
                diaMostrado = true;
            }
        }
        
        if(contador > totalTime && !quitado) {
            quitado = true;
            dayText.SetActive(false);
        }

        else if(!quitado){
            contador += Time.deltaTime;
        }

        if (GameOver) {
            Player.GetComponent<PlayerMovement>().enabled = false;
            Player.GetComponent<ChangeWeapon>().enabled = false;
            Player.GetComponent<attack>().enabled = false;
            Player.GetComponent<LookRotation>().enabled = false;
            Ballesta.SetActive(false);
            Bazooka.SetActive(false);
            if (day < 2) {
                dia = " dia ";
            }
            if(monstruosMatados == 1) {
                monstruo = " monstruo.";
            }
            result.GetComponent<Text>().text = "Has sobrevivido " + day  + dia  + "y \nhas vencido " +  monstruosMatados  + monstruo;
            OverText.SetActive(true);
            result.SetActive(true);
        }
	}

    public void showGameOverText() {

    }
    
    public void mostrarDia() {
        dayText.SetActive(true);
        day++;
        dayText.GetComponent<Text>().text = "Dia " + day;
        contador = 0;
        quitado = false;
    }

    public void actualizarMisiles() {
        misils.GetComponent<Text>().text = "x " + numMisiles;
    }
}
