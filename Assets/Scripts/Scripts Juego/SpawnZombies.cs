﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZombies : MonoBehaviour {

    [SerializeField] GameObject example;
    [SerializeField] GameObject father;
    [SerializeField] GameObject zombiePrefab;
    [SerializeField] GameObject canvas;
    private GameObject zombie;
    private float count;
    private float minTime;
    private float maxTime;
    private float totalTime;
    private bool reducido;

    // Use this for initialization
    void Start () {
        minTime = 40;
        maxTime = 60;
        totalTime = Random.Range(minTime, maxTime);
        count = totalTime;
        reducido = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (canvas.GetComponent<canvasController>().noche) {
            reducido = false;
            if (count >= totalTime) {
                spawnNewZombie();
                count = 0;
                totalTime = Random.Range(minTime, maxTime);
            }

            else {
                count += Time.deltaTime;
            }
        }

        else {
            if (!reducido) {
                reducido = true;
                count = totalTime + 1;
                maxTime -= 5;
                minTime -= 5;
                if(maxTime < 10) {
                    maxTime = 10;
                }
                if(minTime < 5) {
                    minTime = 5;
                }
            }
        }
    }

    void spawnNewZombie() {
        zombie = (GameObject)Instantiate(zombiePrefab, example.transform.position, example.transform.rotation);
        zombie.transform.parent = father.transform;
    }
}
