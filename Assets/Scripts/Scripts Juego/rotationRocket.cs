﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotationRocket : MonoBehaviour {

    [SerializeField] GameObject Father;
    private LookRotation script;    
    
    // Use this for initialization
    void Start () {
        script = Father.GetComponent<LookRotation>();
    }
	
	// Update is called once per frame
	void Update () {
        GetComponent<Transform>().localRotation *= Quaternion.Euler(-script.axisRotation.y, 0, 0);
        GetComponent<Transform>().localRotation = ClampRotationAroundXAxis(GetComponent<Transform>().localRotation);
    }


    Quaternion ClampRotationAroundXAxis(Quaternion q)  {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        script.angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        script.angleX = Mathf.Clamp(script.angleX, script.minAngle, script.maxAngle);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * script.angleX);

        return q;
    }
}
