﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class recogerObjeto : MonoBehaviour {

    private GameObject canvas;
    private GameObject canvasImage;
    private bool destroyObject;
    private float count;
    private bool subido;

	// Use this for initialization
	void Start () {
        canvas = GameObject.FindGameObjectWithTag("canvas");
        canvasImage = GameObject.FindGameObjectWithTag("canvasImage");
        destroyObject = false;
        subido = false;
        count = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (destroyObject) {
            count += Time.deltaTime;
            if(count > 0.2f) {
                Destroy(this.gameObject);
            }
        }
	}

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player") {
            if (!subido) { 
                if(this.gameObject.tag == "rocketAmmo") {
                    this.gameObject.GetComponent<AudioSource>().Play();
                    canvas.GetComponent<canvasController>().numMisiles += 6;
                    canvas.GetComponent<canvasController>().actualizarMisiles();
                    subido = true;
                }
                else {
                    subido = true;
                    this.gameObject.GetComponent<AudioSource>().Play();
                    if (canvasImage.GetComponent<Image>().fillAmount + 0.3f > 1) {
                        canvasImage.GetComponent<Image>().fillAmount = 1;
                    }
                    else {
                        canvasImage.GetComponent<Image>().fillAmount += 0.3f;
                    }
                }
            }
            destroyObject = true;
            //Destroy(this.gameObject);
        }
    }
}
