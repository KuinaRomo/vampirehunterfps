﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnVampire : MonoBehaviour {

    [SerializeField] GameObject objectivePoint;
    [SerializeField] GameObject vampirePrefab;
    [SerializeField] GameObject father;
    [SerializeField] GameObject canvas;
    private float count;
    private float time;
    GameObject vampire;
    public float minTime;
    public float maxTime;
    private bool reducido;

    // Use this for initialization
    void Start () {
        count = 0;
        minTime = 60;
        maxTime = 90;
        reducido = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (canvas.GetComponent<canvasController>().noche) {
            reducido = false;
            if (count > time) {
                count = 0;
                time = Random.Range(minTime, maxTime);
                spawnNewVampire();
            }
            else {
                count += Time.deltaTime;
            }
        }

        else {
            if (!reducido) {
                reducido = true;
                count = time + 1;
                maxTime -= 5;
                minTime -= 5;
                if (maxTime < 10) {
                    maxTime = 10;
                }
                if (minTime < 5) {
                    minTime = 5;
                }
            }
            
        }
	}

    void spawnNewVampire() {
        vampire = (GameObject)Instantiate(vampirePrefab, this.transform.position, this.transform.rotation);
        vampire.GetComponent<moveVampire>().point = objectivePoint;
        vampire.GetComponent<moveVampire>().move();
        vampire.transform.parent = father.transform;
    }
}
