﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class attack : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        Debug.Log("Detecto colision");
        if(other.gameObject.tag == "vampire" || other.gameObject.tag == "zombie") {
            if(other.gameObject.tag == "vampire") {
                other.GetComponent<moveVampire>().count = 0;
                other.GetComponent<moveVampire>().atacando = true;
            }
            else {
                other.GetComponent<moveZombie>().count = 0;
                other.GetComponent<moveZombie>().atacando = true;
            }
            other.gameObject.GetComponent<NavMeshAgent>().speed = 0;
            other.gameObject.GetComponent<Animator>().SetBool("attack", true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "vampire" || other.gameObject.tag == "zombie") {
            if(!other.gameObject.GetComponent<Animator>().GetBool("death")){
                if(other.gameObject.tag == "vampire") {
                    other.gameObject.GetComponent<NavMeshAgent>().speed = other.gameObject.GetComponent<moveVampire>().speed;
                    other.GetComponent<moveVampire>().atacando = false;
                }
                else {
                    other.GetComponent<moveZombie>().atacando = false;
                    other.gameObject.GetComponent<NavMeshAgent>().speed = other.gameObject.GetComponent<moveZombie>().speed;
                }
                other.gameObject.GetComponent<Animator>().SetBool("attack", false);
            }
        }
    }
}
