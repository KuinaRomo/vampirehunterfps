﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRandom : MonoBehaviour {

    public List<GameObject> points;
    private int actualPoint;
    private int random;
    
	// Use this for initialization
	void Start () {
        points = new List<GameObject>();
        for(int i = 0; i < this.transform.childCount; i++) {
            points.Add(this.transform.GetChild(i).gameObject);
        }
        actualPoint = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject changePoint(GameObject lastPoint, bool isIn) {
        if (isIn) { 
            for (int i = 0; i < points.Count; i++) {
                if(points[i] == lastPoint) {
                    actualPoint = i;
                }
            }
        }

        random = actualPoint;
        while(random == actualPoint) {
            random = Random.Range(0, points.Count);
        }

        return points[random];
        
    }
}
