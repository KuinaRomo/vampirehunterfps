﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveRocket : MonoBehaviour {

    public float rocketImpulse;
    public bool ejemplo;
    private bool clavado;
    [SerializeField] GameObject fireEffect;
    [SerializeField] GameObject canvas;
    private float totalTime = 10;
    private float count = 0;

    // Use this for initialization
    void Start () {
        clavado = false;
        canvas = GameObject.FindGameObjectWithTag("canvas");
    }
	
	// Update is called once per frame
	void Update () {
        if (!ejemplo) {
            if (count < totalTime) {
                count += Time.deltaTime;
            }
            else {
                Destroy(this.gameObject);
            }
        }
        
        if (!ejemplo && !clavado) {
            if(this.gameObject.tag == "arrow") {
                GetComponent<Rigidbody>().AddForce(transform.forward * rocketImpulse, ForceMode.Impulse);
            }
            else {
                GetComponent<Rigidbody>().AddForce(transform.forward * -1 * rocketImpulse, ForceMode.Impulse);
            }
        }
    }

    private void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == "heart" || collider.gameObject.tag == "head") {
            if (this.gameObject.tag == "arrow") {
                collider.transform.parent.GetComponent<Animator>().SetBool("death", true);
                this.transform.parent = collider.transform.GetChild(2).transform;
                canvas.GetComponent<canvasController>().monstruosMatados += 1;
            }

            else {
                GameObject fire = Instantiate(fireEffect, collider.transform.position, Quaternion.identity);
                fire.AddComponent<RemoveExplosion>();
                Destroy(this.gameObject);
                Destroy(collider.gameObject);
                canvas.GetComponent<canvasController>().monstruosMatados += 1;
            }
           
        }

        if(collider.gameObject.tag == "vampire" || collider.gameObject.tag == "zombie") {
            if (this.gameObject.tag == "arrow") {
                this.transform.parent = collider.transform.GetChild(2).transform;
            }
            else {
                GameObject fire = Instantiate(fireEffect, collider.transform.position, Quaternion.identity);
                fire.AddComponent<RemoveExplosion>();
                Destroy(this.gameObject);
                Destroy(collider.gameObject);
                canvas.GetComponent<canvasController>().monstruosMatados += 1;
            }
        }

        if(this.gameObject.tag == "arrow") {
            if(collider.gameObject.tag != "Player") {
                clavado = true;
                GetComponent<Rigidbody>().AddForce(Vector3.zero);
                GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }

        else {
            if (collider.gameObject.tag != "Player") {
                Destroy(this.gameObject);
            }
        }
        
    }
}
