﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootRocket : MonoBehaviour {

    [SerializeField] GameObject rocket;
    [SerializeField] GameObject finalParent;
    private GameObject rocketCreated;
    [SerializeField] GameObject ejemplo;
    [SerializeField] GameObject canvas;
    
    
    // Use this for initialization
    void Start() {
        
    }

    // Update is called once per frame
    void Update()  {
        if (Input.GetMouseButtonDown(1)) {
            Debug.Log("¿Que tag es? " + rocket.gameObject.tag);
            if(rocket.gameObject.tag == "rocket" && canvas.GetComponent<canvasController>().numMisiles > 0 
                || rocket.gameObject.tag == "arrow") { 
                rocketCreated = (GameObject)Instantiate(rocket, ejemplo.transform.position, ejemplo.transform.rotation);
                if(rocketCreated.gameObject.tag == "rocket") {
                    canvas.GetComponent<canvasController>().numMisiles -= 1;
                    canvas.GetComponent<canvasController>().actualizarMisiles();
                }
                rocketCreated.transform.parent = this.transform; 
                rocketCreated.transform.position = new Vector3(ejemplo.transform.position.x, ejemplo.transform.position.y, ejemplo.transform.position.z);
                rocketCreated.transform.parent = finalParent.transform;
            }
        }
    }
}
